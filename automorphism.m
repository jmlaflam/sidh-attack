// Adjust avisogenies path
import "../avisogenies/src/Arithmetic.m" : init_theta_null_point, init_theta_point,print_point,compare_points;
import "../avisogenies/src/rosenhain.m" : sqlvl22_to_lvl2;


/*given the gaudry index of a zero coordinate of a theta null point
  gives a matrix that will map that coordinate to [1,1,1,1]*/
function get_aut_matrix(i)
    case i:
        when 1:
            g:= Matrix(4,4,[1,1,0,-1,0,1,0,-1,-1,0,1,0,1,0,-1,1]);
        when 2:
            g:= Matrix(4,4,[1,1,1,0,0,1,0,0,1,0,2,0,-1,-1,-2,1]);
        when 3:
            g:= Matrix(4,4,[1,0,0,-1,0,1,-1,1,-1,-1,2,0,-1,0,0,2]);
        when 4:
            g:= Matrix(4,4,[1,1,1,1,0,1,1,0,-1,0,1,-1,1,2,1,2]);
        when 5:
            g:= Matrix(4,4,[1,-1,0,-1,0,1,0,1,-1,1,1,1,0,0,1,1]);
        when 6:
            g:= Matrix(4,4,[1,1,-1,0,0,1,1,-1,0,0,1,0,0,-1,-2,2]);
        when 7:
            g:= Matrix(4,4,[1,0,-1,0,-1,1,1,-1,-1,0,2,1,0,0,0,1]);
        when 8:
            g:= Matrix(4,4,[1,0,0,1,0,1,1,-1,1,0,1,1,0,0,0,1]);
        when 9:
            g:= Matrix(4,4,[1,0,0,0,1,1,0,0,0,0,1,-1,0,0,0,1]);
        when 10:
            g:=IdentityMatrix(Integers(),4);
    end case;
    return g;
end function;


/*Returns a unit root in the finite field F that corresponds to
 the unit root e^(I*pi*exp) in the complex plane*/
function get_unit_root(exp, field)
    F<I>:=field;
    irt2:=(F!1)/Sqrt(F!2);
    while exp le -1 do
        exp+:=2;
    end while;
    while exp gt 1 do
        exp-:=2;
    end while;
    case exp:
        when 0:
            return F!1;
        when 1/4:
            return irt2+irt2*I;
        when 1/2:
            return I;
        when 3/4:
            return -irt2+irt2*I;
        when 1:
            return F!-1;
        when -1/4:
            return irt2-irt2*I;
        when -1/2:
            return -I;
        when -3/4:
            return -irt2-irt2*I;
    end case;
end function;


/*Gives the Gaudry numbering of a coordinate see 
  https://theses.hal.science/tel-00642951/file/main.pdf p.37*/ 
function gaudry_numbering(v)
    case v:
        when [0,0,0,0]:
            return 1;
        when [0,0,0,1]:
            return 4;
        when [0,0,1,0]:
            return 3;
        when [0,0,1,1]:
            return 2;
        when [0,1,0,0]:
            return 7;
        when [0,1,0,1]:
            return 11;
        when [0,1,1,0]:
            return 9;
        when [0,1,1,1]:
            return 12;
        when [1,0,0,0]:
            return 5;
        when [1,0,0,1]:
            return 6;
        when [1,0,1,0]:
            return 13;
        when [1,0,1,1]:
            return 15;
        when [1,1,0,0]:
            return 8;
        when [1,1,0,1]:
            return 16;
        when [1,1,1,0]:
            return 14;
        when [1,1,1,1]:
            return 10;
    end case;
end function;
    
    

/* Checks if a matrix is in the symplectic gorup SP(4,Z)
   It was used to generate the matrices in get_aut_matrix */
function in_sp_2gZ(G)
    A:=Submatrix(G,1,1,2,2);
    B:=Submatrix(G,1,3,2,2);
    C:=Submatrix(G,3,1,2,2);
    D:=Submatrix(G,3,3,2,2);
    
    if IsSymmetric(Transpose(A)*C) and IsSymmetric(Transpose(D)*B) and IsIdentity(Transpose(A)*D-Transpose(C)*B) then
        return true;
    else
        return false;
    end if;  
end function;



/* Applies the automorphism gamma to the theta point y
   Q0 is the theta null point of the structure on which y lies*/
function apply_automorphism(g,Q0,y)
    Fp2<I>:=Parent(Q0`coord[0]);
    //sth:=lvl2_to_sqlvl22(y`coord,Q0`coord);
    DDg:=AbelianGroup([2: i in [1..2*2]]);
    if compare_points(y,Q0) then
        yan:=AlgebraicToAnalyticThetaNullPoint(2,Q0,DDg);
    else
        yan:=AlgebraicToAnalyticThetaPoint(2,Q0,y,DDg);
    end if;
    sth:= yan`coord;
    Ab:=Universe(sth);
    gsth:=sth;
    
    A:=Submatrix(g,1,1,2,2);
    B:=Submatrix(g,1,3,2,2);
    C:=Submatrix(g,3,1,2,2);
    D:=Submatrix(g,3,3,2,2);
    ep:=Diagonal(Transpose(A)*C);
    epp:=Diagonal(Transpose(D)*B);
    ep:=[x mod 2: x in ep];
    epp:=[x mod 2: x in epp];
    e:=Matrix(FiniteField(2),4,1,ep cat epp);

    ep:=1/2*Matrix(Rationals(),2,1,ep);
    epp:=1/2*Matrix(Rationals(),2,1,epp);
    
    gam:=ChangeRing(g,FiniteField(2));
    A:=ChangeRing(A,Rationals());
    B:=ChangeRing(B,Rationals());
    C:=ChangeRing(C,Rationals());
    D:=ChangeRing(D,Rationals());
    for v in Ab do
        newv:=Eltseq(Transpose(gam)*Matrix(FiniteField(2),4,1,Eltseq(v))+e);
    
        a:=1/2*Matrix(Rationals(),2,1,Eltseq(v)[1..2]);
        b:=1/2*Matrix(Rationals(),2,1,Eltseq(v)[3..4]);
        
        exp:=-(Transpose(a)*A*Transpose(B)*a)[1][1];
        zeta:=get_unit_root(exp,Fp2);
        
        exp:=-(Transpose(b)*C*Transpose(D)*b)[1][1];
        zeta*:=get_unit_root(exp,Fp2);
        
        exp:=-2*(Transpose(a)*B*Transpose(C)*b)[1][1];
        zeta*:=get_unit_root(exp,Fp2);
        
        exp:=-2*(Transpose(Transpose(A)*a+Transpose(C)*b+ep)*epp)[1][1];
        
        zeta*:=get_unit_root(exp,Fp2);
        gsth[v]:=zeta^2*sth[newv];
    end for;
    ab2:=Universe(Q0`coord);
    Z2:=sqlvl22_to_lvl2(gsth,ab2);
    
    return Z2;
end function;