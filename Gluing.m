// Adjust avisogenies path
import "../avisogenies/src/Arithmetic.m" : init_theta_null_point, init_theta_point;


/* INPUTS: -- E: an elliptic curve
           -- Ab: Abelian Group used to index the theta coordinates of E 
                  it should be Z/2Z
  OUTPUTS: -- The theta coordinates (in level 2) of E (as an associative array indexed by Z/2Z)
           -- A list containing the roots of the polynomial f(x) where E: y^2=f(x)*/
function EllToTheta(E,Ab)
    F<I> := BaseField(E);
    //FF<w>:= ext< F | 2 >;
    R<x> := PolynomialRing(F);

    f, _ := HyperellipticPolynomials(E);
    
    r := Roots(f);
    e1 := F!r[1][1];
    e2 := F!r[2][1];
    e3 := F!r[3][1];

    Z2:=AssociativeArray(Ab);
    
    Z2[0]:=Sqrt(e1-e3)+Sqrt(e1-e2);
    Z2[1]:=Sqrt(e2-e3);
    return Z2,[e1,e2,e3];
end function;


/* INPUTS: -- th: a tuple which has the form <theta coordinates of P1, theta coordinates of P2>
           -- Ab: Abelian Group used to index the theta coordinates of (P1,P2) 
                  it should be Z/2ZxZ/2Z
   OUTPUT: The theta coordinates (in level 2) of (P1,P2) (as an associative array indexed by Ab)*/
function ThetaProduct(th,Ab)
    D1 := Universe(th[1]);
    D2 := Universe(th[2]);
    F := Parent(th[1][0]);
    
    thProd := AssociativeArray(Ab);
    
    thProd[Ab![0,0]] := th[1][D1!0]*th[2][D2!0];
    thProd[Ab![0,1]] := th[1][D1!0]*th[2][D2!1];
    thProd[Ab![1,0]] := th[1][D1!1]*th[2][D2!0];
    thProd[Ab![1,1]] := th[1][D1!1]*th[2][D2!1];

    return thProd;
end function;


/* INPUTS: -- P: a point on an elliptic curve E
           -- thE: the theta null point of E (as a rec: Algebraic theta point)
           -- r: the roots of the polynomial f(x) where E: y^2=f(x)
   OUTPUT: The theta coordinates (in level 2) of P (as an associative array indexed by Z/2Z)*/
function EllPtToTheta(P,thE,r)
    Ab:=AbelianGroup([2,2]);
    A:=AbelianGroup([2]);
    
    // We convert P to the correct format for input to MumfordToLevel2ThetaPoint
    if P[3] eq 0 then
        points := [];
    else
        points := [[P[1], P[2]]];
    end if;
    
    // We need to convert thE to an analytic theta point to use it in MumfordToLevel2ThetaPoint
    thEAn := AlgebraicToAnalyticThetaNullPoint(1,thE,Ab);
    P := MumfordToLevel2ThetaPoint(1, r, thEAn, [p : p in points]);
    P := AnalyticToAlgebraicThetaPoint(1,P,A);
    
    return P`coord;
end function;


/* INPUTS: -- K: an associative array containing points on the product of
              two elliptic curves E1 x E2 
           -- thE: A list of the form [theta null point of E1, theta null point of E2]
              (where the theta null points are given as a rec: Algebraic theta point)
           -- r: A list of the form [roots of f1(x), roots of f2(x)]
                 (where E1: y^2=f1(x) and E2: y^2=f2(x))
           -- Ab: Abelian Group used to index the theta coordinates of (P1,P2) 
                  it should be Z/2ZxZ/2Z
   OUTPUT: An associative array thK with the same indices as K where thK[i] contains
           the theta point (in level 2) corresponding to the point K[i]*/
function KernelToTheta(K,thE,r,Ab)
    g := 2;
    
    Dl := Universe(K);
    thK := AssociativeArray(Dl);
    for k in Keys(K) do
        Pt := K[k];
        thEP := [EllPtToTheta(Pt[i],thE[i],r[i]): i in [1,2]];
        thK[k] := init_theta_point(ThetaProduct(thEP,Ab));
    end for;
    return thK;
end function;






    



