
clear;
import "Splitting.m":ThetaToEll,ThetaToEllPt;
import "Gluing.m":ThetaProduct,EllToTheta,KernelToTheta;
import "ImagePoint.m":ImagePointTheta;
import "automorphism.m":gaudry_numbering,get_aut_matrix,apply_automorphism;
import "../avisogenies/src/Arithmetic.m" : precompute_D,init_theta_point, init_theta_null_point,print_point;
import "../avisogenies/src/Isogenies.m" : compute_all_theta_torsion;
import "../avisogenies/src/rosenhain.m" : theta_null_to_rosenhain,precompute_mumford_to_theta,mumford_to_theta;


/******************************************************************************
The first section of this file was copied from the file "SIKEp751_attack.m" at 
https://github.com/KULeuven-COSIC/3_3_isogenies and adapted slightly
*******************************************************************************/

lA:=2;
lB:=5;
a:=93;
b:=104;
d:=1;
p:=lA^a*lB^b-1;
u:=2186748323477221906254017627751826127;
v:=192686550776697339398400691496753224;

Z:=Integers();
Fp2<I> := GF(p, 2);
R<x> := PolynomialRing(Fp2);

//This is the starting curve in the SIKE protocol for all security levels.
E_start := EllipticCurve(x^3 + 6*x^2 + x);
infty := E_start ! 0;


// We (naively) compute lA^a- and lB^b-torsion bases instead of hardcoding them.
repeat PA := Z!((p+1)/lA^a)*Random(E_start);
until lA^(a-1)*PA ne infty;
repeat QA := Z!((p+1)/lA^a)*Random(E_start);
until WeilPairing(PA, QA, lA^a)^(lA^(a-1)) ne 1;

repeat PB := Z!((p+1)/lB^b)*Random(E_start);
until lB^(b-1)*PB ne infty;
repeat QB := Z!((p+1)/lB^b)*Random(E_start);
until WeilPairing(PB, QB, lB^b)^(lB^(b-1)) ne 1;

/*
We will now precompute the (u+2*v*i)-endomorphism used in the attack SIKEp751.
Remark that this is NOT part of the standard set-up. However, it can be done offline so we ignore it for timing purposes.
Either way, this computation takes no time whatsoever.
*/

// We first compute the needed two_i endomorphism (naively but efficient)
E1728, phi := IsogenyFromKernel(E_start, x);
for iota in Automorphisms(E1728) do
  P := Random(E1728);
  if iota(iota(P)) eq -P then
    two_i := phi*iota*DualIsogeny(phi);
    break;
  end if;
end for;

// Sanity check:
assert lB^b - d*lA^a eq (u+2*v*I)*(u-2*v*I);

/*
We now define the public lA^a- and lB^b-torsion points that Alice and Bob work with.
Remark that we worked the opposite route and typically this is given first, and not the points after the endomorphism.
Again, this doesn't matter since it takes no time and can be done in a precomputation.
*/
PA_pub := u*PA + v*two_i(PA);
QA_pub := u*PA + v*two_i(QA);
PB_pub := u*PB + v*two_i(PB);
QB_pub := u*QB + v*two_i(QB);


/*
The following function lets Alice generate a random lA^a-subgroup and execute her first part of the SIKE protocol.
*/
function Alices_computation(E_start, PA_pub, QA_pub, PB_pub, QB_pub)
	repeat k_A_private := Random(lA^a);
	KA := PA_pub + k_A_private*QA_pub;
	until lA^(a-1)*KA ne infty;

	// Naive implementation of lA^a-isogeny computation but fast enough for testing
	order := lA^a;
	EA := E_start;
	phiPB := PB_pub;
	phiQB := QB_pub;
	while order ne 1 do
		local_kernel := (order div lA)*KA;
		EA, phiA := IsogenyFromKernel(EA, &*[x-(i*local_kernel)[1] : i in [1..lA div 2]]);
		order div:= lA;
		KA := phiA(KA);
		phiPB := phiA(phiPB);
		phiQB := phiA(phiQB);
	end while;

	return k_A_private, EA, phiPB, phiQB;

end function;
k_A_private, EA, phiPB, phiQB := Alices_computation(E_start, PA_pub, QA_pub, PB_pub, QB_pub);

/*
Function to extend Alice's isogeny by an arbitrary isogeny of degree d.
There is rational 10-torsion on the twist which allows us to exploit x-only arithmetic over Fp2.
*/

function extend_phi(EA, phiPB, phiQB)

    divsd:=Divisors(d);Remove(~divsd,#divsd);
	repeat Pd := (((p+1) div d)*Random(EA)); until EA ! 0 notin {f*Pd: f in divsd};
	EA_extd, phid := IsogenyFromKernel(EA, &*[x-(i*Pd)[1] : i in [1..d div 2]]);
	phid_PB := phid(phiPB);
	phid_QB := phid(phiQB);
	
	return EA_extd, phid_PB, phid_QB;
end function;

/*
Computing (lA^a*PB, -phi_extPB), (lA^a*QB, -phi_extQB) and renaming it (R1,S1) and (R2,S2);
*/

function get_Ri_Si(PB, phi_extPB, QB, phi_extQB);
	R1 := d*lA^a*PB;
	S1 := -phi_extPB;
	R2 := d*lA^a*QB;
	S2 := -phi_extQB;
	return R1, S1, R2, S2;
end function;


/*
Function to generate a lA^a-torsion basis for EA.
*/

function get_lA_torsion_EA_ext(EA)
	repeat
		PA_cod := Z!((p+1)/lA^a)*Random(EA);
		QA_cod := Z!((p+1)/lA^a)*Random(EA);
	until WeilPairing(PA_cod, QA_cod, lA^a)^(lA^(a-1)) ne 1;
	return PA_cod, QA_cod;
end function;

function verify_attack(P,EA)
	E_ := E_start;
	Prem := P;
	order := lA^a;
	while order ne 1 do
		local_kernel := (order div lA)*Prem;
		if local_kernel ne E_ ! 0 then
			E_, phiA := IsogenyFromKernel(E_, &*[x-(i*local_kernel)[1] : i in [1..lA div 2]]);
			Prem := phiA(Prem);
		end if;
		order div:= lA;
	end while;
	j1 := jInvariant(EA); j2 := jInvariant(E_);
	/*
	If we extend by a 2-isogeny which is part of the dual, we are
	"unlucky" but can still verify easily with the modular polynomial.
	*/
	class2 := ClassicalModularPolynomial(lA);
	isomorph := j1 eq j2;
	two_isog := Evaluate(class2,[j1,j2]) eq 0;
	return isomorph or two_isog;
end function;

/******************************************************************************
The following functions make use of the package avisogenies 
https://gitlab.inria.fr/roberdam/avisogenies/ to compute an (lB,lB)-isogeny chain
*******************************************************************************/

/* INPUTS: -- R1,R2: points on the elliptic curve E_start
           -- S1,S2,P,Q: points on the elliptic curve EA
           
              The kernel of the isogeny is <(R1,S1),(R2,S2)> and we want to
              map the points (infty,P) and (infty,Q) through the chain
              
  OUTPUTS: -- The theta null point Q0 of the jacobian after the first gluing step
           -- A list containing the theta points corresponding to the images
              of (R1,S1),(R2,S2),(infty,P) and (infty,Q) through the glue isogeny*/
function glue_evaluation(R1,S1,R2,S2,P,Q)
    E1:=Curve(R1); E2:=Curve(S1);
    // Compute theta null points of E1 and E2
    Ab2:= AbelianGroup([2]);
    thE1,r1:=EllToTheta(E1,Ab2);
    thE2,r2:=EllToTheta(E2,Ab2);
    thE:=[init_theta_null_point(thE1),init_theta_null_point(thE2)];
    r:=[r1,r2];
    
    // Compute theta null point of product E1 x E2
    Ab22:=AbelianGroup([2,2]);
    P0:=init_theta_null_point(ThetaProduct([thE1,thE2],Ab22));
    
    // Compute the first kernel
    KR1:=lB^(b-1)*R1; KS1:=lB^(b-1)*S1;
    KR2:=lB^(b-1)*R2; KS2:=lB^(b-1)*S2;
    
    Dll:= AbelianGroup([lB+1,lB+1,lB+1]);
    Dl:= AbelianGroup([lB+1,lB+1]);
    K:=AssociativeArray(Dl);
    L:=AssociativeArray(Dll);
    for i in [0..lB] do
        K[[i,0]]:=<i*KR1,i*KS1>;
        K[[0,i]]:=<i*KR2,i*KS2>;
    end for;
    for i in [1..lB] do
        for j in [1..lB] do
            K[[i,j]]:= <K[[i,0]][t]+K[[0,j]][t]:t in [1,2]>;
        end for;
    end for;
    
    // Convert kernel to theta coordinates
    thK:=KernelToTheta(K,thE,r,Ab22);
    for dl in Dl do
        dll:=Eltseq(dl);
        Append(~dll,0);
        L[dll]:=thK[dl];
    end for;
    L[0]:=P0;
    
    //Compute image of points
    points:=[<R1,S1>,<R2,S2>,<E1!0,P>,<E1!0,Q>];
    codomain_points:=[];
    DDg:=AbelianGroup([2: i in [1..2*2]]);
    first:=true;
    for pt in points do
        //Compute pt+k for all k in the kernel
        L1:=AssociativeArray(Dll);
        for k in [1..lB] do
            kpt:=<k*pt[i]:i in [1,2]>;
            for i in [0..lB] do
                for j in [0..lB] do
                    L1[[i,j,k]]:=<K[[i,j]][t]+kpt[t]:t in [1,2]>;
                end for;
            end for;
        end for;
        
        // Convert these to theta coordinates
        thL1:=KernelToTheta(L1,thE,r,Ab22);
        for dl in Keys(thL1) do
            L[Eltseq(dl)]:=thL1[dl];
        end for;
        
        // Compute the isogeny in theta coordinates
        L:=compute_all_theta_torsion(L);
        y,Q0:=ImagePointTheta(L,lB);
        Append(~codomain_points,y);
    end for;
    
    return Q0,codomain_points;   
end function;

/* INPUTS: -- P0: the theta null point of a Jacobian
           -- th_points: A list containing the theta points corresponding to the images
              of (R1,S1),(R2,S2),(infty,P) and (infty,Q) through the first b-bi steps
           -- bi: the order of the kernel points. We are at the (b-bi)th step of the chain
  OUTPUTS: -- The theta null point Q0 of the jacobian after the first b-bi steps
           -- A list containing the theta points corresponding to the images
              of (R1,S1),(R2,S2),(infty,P) and (infty,Q) through the first b-bi steps */
function FromJacToJac(P0,th_points,bi)
    // Convert Q0 to jacobian 
    DDg:=AbelianGroup([2,2,2,2]);
    P0an:=AlgebraicToAnalyticThetaNullPoint(2,P0,DDg);
    ros:=theta_null_to_rosenhain(P0);
    AK<X>:=PolynomialRing(Universe(ros));
    f:=&*[X-i: i in ros];
    H:=HyperellipticCurve(f);
    J := Jacobian(H);
    
    // Convert theta points to divisors on J to compute next kernel
    points:=[];
    for y in th_points do
        Pan:=AlgebraicToAnalyticThetaPoint(2,P0,y,DDg);
        u,v2 := Level2ThetaPointToMumford(2,ros,P0an,Pan);
        v := Sqrt(v2);
        ymumford := J ! [u,v];
        Append(~points,ymumford);
    end for;
    
    K:=[lB^(bi-1)*points[1], lB^(bi-1)*points[2]];
    
    // Some precomputations for conversion
    precomp:=AssociativeArray();
    precompH:=AssociativeArray();
    D:=Universe(th_points[1]`coord);
    precompute_D(D,~precomp);
    FK:=BaseField(H);
    precompute_mumford_to_theta(~precomp,H,FK);
    
    // Compute images of points
    codomain_points:=[];
    Dll:=AbelianGroup([lB+1: i in [1..3]]);
    L:=AssociativeArray(Dll);
    L[Dll.0]:=precomp["HK","P0"];
    for i in [1,2] do
        L[Dll.i]:=mumford_to_theta(K[i],precomp);
    end for;
    L[Dll.1+Dll.2]:=mumford_to_theta(K[1]+K[2],precomp);
    
    for i in [1..4] do
        L[Dll.3]:=mumford_to_theta(points[i],precomp);
        for j in [1,2] do
            L[Dll.3+Dll.j]:=mumford_to_theta(points[i]+K[j],precomp);
        end for;
        L:=compute_all_theta_torsion(L);
        y,Q0:=ImagePointTheta(L,lB);
        Append(~codomain_points,y);
    end for;
    
  return Q0, codomain_points;
end function;

/* INPUTS: -- Q0: the theta null point of the product of elliptic curves E0' x C'
           -- y: the theta point of a point (P0,PC) on E0' x C' 
   OUTPUT: -- The point P0 on E0' */
function recover_phi_hat(y,Q0)

    // Apply automorphism
    DDg:=AbelianGroup([2,2,2,2]);
    Q0an:=AlgebraicToAnalyticThetaNullPoint(2,Q0,DDg);
    for v in DDg do
        i:=gaudry_numbering(Eltseq(v));
        if Q0an`coord[v] eq 0 and i le 10 then
            zero_coord:=i;
        end if;
    end for;
    gamma:=get_aut_matrix(zero_coord);
    newQ0:=apply_automorphism(gamma,Q0,Q0);
    newy:=apply_automorphism(gamma,Q0,y);
    
    // Recover the elliptic curve
    Ab:=AbelianGroup([2]);
    thE:=AssociativeArray(Ab);
    thP:=AssociativeArray(Ab); thP[1]:=1;
    thE[0]:= newQ0[[0,0]]/newQ0[[1,0]]; thE[1]:= 1;
    E,rac:=ThetaToEll(thE);
    
    // Recovering the theta coordinates of the elliptic curve point
    if jInvariant(E) eq jInvariant(E_start) then
        thP[0]:= newy[[0,0]]/newy[[1,0]];
    else;
        // If E is not isomorphic to E_start, we want the second curve
        thE[0]:= newQ0[[0,0]]/newQ0[[0,1]];
        E,rac:=ThetaToEll(thE);
        thP[0] := newy[[0,0]]/newy[[0,1]];
    end if;
    
    // Recover elliptic curve point from its theta coordinates
    thE:=init_theta_null_point(thE);
    thP:=init_theta_point(thP);
    return ThetaToEllPt(thP,thE,E,rac);
end function;



/* INPUTS: -- P0: the theta null point of a Jacobian
           -- th_points: A list containing the theta points corresponding to the images
              of (R1,S1),(R2,S2),(infty,P) and (infty,Q) through all the chain except the splitting
  OUTPUTS: -- A list containing the points phiAhat(PA_cod) and phiAhat(QA_cod) on E0' */
function split_evaluation(P0, th_points)
    // Convert Q0 to jacobian 
    DDg:=AbelianGroup([2,2,2,2]);
    P0an:=AlgebraicToAnalyticThetaNullPoint(2,P0,DDg);
    ros:=theta_null_to_rosenhain(P0);
    AK<X>:=PolynomialRing(Universe(ros));
    f:=&*[X-i: i in ros];
    H:=HyperellipticCurve(f);
    J := Jacobian(H);
    
    // Convert theta points to divisors on J to compute next kernel
    points:=[];
    for y in th_points do
        Pan:=AlgebraicToAnalyticThetaPoint(2,P0,y,DDg);
        u,v2 := Level2ThetaPointToMumford(2,ros,P0an,Pan);
        v := Sqrt(v2);
        ymumford := J ! [u,v];
        Append(~points,ymumford);
    end for;
    
    K:=[points[1],points[2]];
    points:=points[3..4];
    
    // Some precomputations for conversion
    precomp:=AssociativeArray();
    precompH:=AssociativeArray();
    D:=Universe(th_points[1]`coord);
    precompute_D(D,~precomp);
    FK:=BaseField(H);
    precompute_mumford_to_theta(~precomp,H,FK);
    
    // Compute images of points
    codomain_points:=[];
    Dll:=AbelianGroup([lB+1: i in [1..3]]);
    L:=AssociativeArray(Dll);
    L[Dll.0]:=precomp["HK","P0"];
    for i in [1,2] do
        L[Dll.i]:=mumford_to_theta(K[i],precomp);
    end for;
    L[Dll.1+Dll.2]:=mumford_to_theta(K[1]+K[2],precomp);
    
    for i in [1..2] do
        L[Dll.3]:=mumford_to_theta(points[i],precomp);
        for j in [1,2] do
            L[Dll.3+Dll.j]:=mumford_to_theta(points[i]+K[j],precomp);
        end for;
        L:=compute_all_theta_torsion(L);
        y,Q0:=ImagePointTheta(L,lB);
    
        Append(~codomain_points,recover_phi_hat(y,Q0));
    end for;
    
	return codomain_points;
end function;

/***************************************************************************/

/*This function is an adaptation of the function of the same name in the file 
"SIKEp751_attack.m" at https://github.com/KULeuven-COSIC/3_3_isogenies
It recovers the generator of the kernel of Alice's isogeny phiA*/
function attack_Alice(E_start, PB, QB, EA, phiPB, phiQB,l)
    if d gt 1 then
        EA, phiPB, phiQB := extend_phi(EA, phiPB, phiQB);
    end if;
	R1, S1, R2, S2 := get_Ri_Si(PB, phiPB, QB, phiQB);
	PA_cod, QA_cod := get_lA_torsion_EA_ext(EA);

	// Gluing
    print("gluing");
	Q0, points := glue_evaluation(R1,S1,R2,S2,PA_cod,QA_cod);
	// (3,3)-isogenies between Jacobians
	for i in [1..b-2] do
        print "Jac to Jac", i;
        Q0, points := FromJacToJac(Q0,points, b-i);
    end for;
	// Splitting
    print("splitting");
	codomain_points := split_evaluation(Q0, points);
    
    // Map the points to E_start
    P1,P2:=Explode(codomain_points);
    _, iso := IsIsomorphic(Curve(P1),E_start);
    P1 := iso(P1);
    _, iso := IsIsomorphic(Curve(P2),E_start);
    P2 := iso(P2);
	if lA^(a-1)*P1 ne E_start ! 0 then P := P1;
	elif lA^(a-1)*P2 ne E_start ! 0 then P := P2;
	elif lA^(a-2)*P1 ne E_start ! 0 then P := P1; // Can happen if we took part of dual in extending Alice's isogeny
	elif lA^(a-2)*P2 ne E_start ! 0 then P := P2;
	else "very strange order..."; P := P1;
	end if;

	return P;

end function;

time P := attack_Alice(E_start, PB, QB, EA, phiPB, phiQB, lB);
assert verify_attack(P,EA);




