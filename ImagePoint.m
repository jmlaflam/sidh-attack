// Adjust avisogenies path
import "../avisogenies/src/Arithmetic.m" : init_theta_point, init_theta_null_point, precompute_D, get_nonzero_coord, compare_points, print_point,diff_mult;
import "../avisogenies/src/libav.m" : GetCastMinField, SumOfSquares;
import "../avisogenies/src/rosenhain.m" : sqlvl22_to_lvl2,theta_null_to_rosenhain, precompute_mumford_to_theta, precompute_H, mumford_to_theta,lvl2_to_mumford;
import "../avisogenies/src/Isogenies.m" : get_coeff_structure, compute_all_theta_torsion, JacobianOrTwist;

/*********************************************************************************************
 This file is a slightly modified version of the file "Image.m" from the avisogenies package as
 it contained minor bugs and also did not allow to compute images in the glue and split cases 
 *********************************************************************************************/
  
function get_image_coeff_structure(l,g)
  Dll:=AbelianGroup([l+1: i in [1..g+1]]);
  return get_coeff_structure(Dll);
end function;

function image_level_formula(D,Dl)
  Inv:=Invariants(D); g:=#Inv; n:=Inv[1];
  assert #Seqset(Inv) eq 1;// we assume that D=[n,n,n,n,...]

  Invl:=Invariants(Dl); l:=Invl[1];
  assert #Seqset(Invl) eq 1;// we assume that Dl=[l,l,l,l,....]
  assert #Invl eq g;

  assert Gcd(l,n) eq 1; // assert that n and l are coprime
  // assert IsPrime(l); // TODO is this needed?

  sumsquares:=SumOfSquares(l);   // We have l=a^2+b^2+c^2+d^2
  sumsquares[1]:=-sumsquares[1]; // In order to have F^TF= l*Id, we need to use -a,b,c,d to build F
  r:=#sumsquares;
  A:=AbelianGroup([l: i in [1..r]]);
  B:=AbelianGroup([n: i in [1..r]]);

  if r eq 1 then
    a:=Explode(sumsquares);
    phi:=hom<A -> A | [a*A.1]>;
    phi2:=hom<B -> B | [a*B.1]>;
    //phi2 is used to compute a section of phi
  elif r eq 2 then
    a,b:=Explode(sumsquares);
    phi:=hom<A -> A | [a*A.1+b*A.2, -b*A.1+a*A.2]>;
    phi2:=hom<B -> B | [a*B.1+b*B.2, -b*B.1+a*B.2]>;
  elif r eq 4 then
    a,b,c,d:=Explode(sumsquares);
    phi:=hom<A -> A | [ a*A.1 + b*A.2 + c*A.3 + d*A.4,
                       -b*A.1 + a*A.2 - d*A.3 + c*A.4,
                       -c*A.1 + d*A.2 + a*A.3 - b*A.4,
                       -d*A.1 - c*A.2 + b*A.3 + a*A.4 ]>;
    phi2:=hom<B -> B | [ a*B.1 + b*B.2 + c*B.3 + d*B.4,
                        -b*B.1 + a*B.2 - d*B.3 + c*B.4,
                        -c*B.1 + d*B.2 + a*B.3 - b*B.4,
                        -d*B.1 - c*B.2 + b*B.3 + a*B.4 ]>;
  else
    error "Cardinality of sumsquares not supported";
  end if;

  vprint AVIsogenies, 7: "Computing Kernel";
  Kset:={A!i: i in Kernel(phi)};

  return Kset, phi, phi2;
end function;

/* Here, the input and outputs were changed to theta coordinates so that the function could be used
   in the glue and split cases.
   
   -- L is an array indexed by Dll = Z/(l+1)Z x Z/(l+1)Z x Z/(l+1)Z with
      L[i*Dll.1+ j*Dll.2 + m*Dll.3] = i*k1 + j*k2 + m*x (in theta coordinates)
      Where K = <k1,k2> is the kernel of the isogeny and x is the point whose image we would like to find
   -- l is the degree of the isogeny */

function ImagePointTheta(L,l :
  proj_coordinate:=0,
  curve_to_invariants:=func<x|G2Invariants(x)>,
  invariants_to_curve:=HyperellipticCurveFromG2Invariants)

  g:=2;
  precomp:=AssociativeArray();
  P0:=L[0];
  D := Universe(P0`coord);
  precompute_D(D,~precomp);
  
  Dl:=AbelianGroup([l,l]);
  gen:=[Dl.i: i in [1..g]] cat [Dl.i+Dl.j: i,j in [1..g] | i lt j];
  Dll:=Universe(L);
  abstract_coeff_x:=get_coeff_structure(Dll);

  function inc1(i)
    el:=Eltseq(i) cat [0];
    return Dll!el;
  end function;
  function proj1(i)
    el:=Eltseq(i)[1..g];
    return Dl!el;
  end function;

  x:=L[Dll.(g+1)];
  

  l1:=l div 2; l11:=l1 +1;
  coeffs1:=[];
  for j in [1..#gen] do
    i:=gen[j];
    l11P:=L[inc1(Dl!Eltseq(l11*i))]`coord;
    l1P:=L[inc1(Dl!Eltseq(l1*i))]`coord;
    i0:=get_nonzero_coord(l11P);
    coeffs1[j]:=l1P[-i0]/l11P[i0];
  end for;
  coeffs2:=[];
  for i in [1..g] do
    lxP:=L[l*Dll.i+Dll.(g+1)]`coord;
    i0:=get_nonzero_coord(lxP);
    coeffs2[i]:=x`coord[i0]/lxP[i0];
    //cf Section 4.2, we need to correct by \lambda_i^(l(l-1))
    coeffs2[i]:=coeffs2[i]/coeffs1[i]^(l-1);
  end for;
  // we simulate a coeff of 1 for x
  coeffs:=coeffs1[1..g] cat [1] cat coeffs1[g+1..#coeffs1] cat coeffs2;

  //Now we do the level formula, which look at lot like an isogeny level formula but for g+1.
  Kset,phi,phi2:=image_level_formula(D,Dl);
  B:=Domain(phi2); //Z/nZ^r
  squares:=SumOfSquares(l);
  r:=#squares;
  res:=AssociativeArray(D);
  resx:=AssociativeArray(D);
  preims:=AssociativeArray(D);
  for i in D do
    res[i]:=0;
    resx[i]:=0;
    I:=Eltseq(i);
    preim:=[D.0: i in [1..r]];
    for z in [1..g] do
      preimz:=Eltseq((B!([I[z]] cat [proj_coordinate: zz in [1..r-1]]))@@phi2);
      // preimz is a preimage on the z coordinate
      preim:=[ preim[i]+preimz[i]*D.z: i in [1..r]];
      
    end for;
    preims[i]:=preim;
  end for;
  
  preimx:=squares;

  for tuple in CartesianPower(Kset,g) do
    k:=[ &+[Eltseq(tuple[z])[i]*Dl.z: z in [1..g]]: i in [1..r]];
    k:=[inc1(z): z in k];
    kx:=[squares[i]*Dll.(g+1)+k[i]: i in [1..r]];
    s1:=Eltseq(&+[abstract_coeff_x[z]: z in k]);
    s1x:=Eltseq(&+[abstract_coeff_x[z]: z in kx]);
    s:=[]; sx:=[];
    for i in [1..#s1] do
      test,ss:=IsDivisibleBy(s1[i],l);
      assert test;
      test,ssx:=IsDivisibleBy(s1x[i],l);
      assert test;
      Append(~s,ss);
      Append(~sx,ssx);
    end for;

    c:=&*[coeffs[i]^s[i]: i in [1..#s]];
    cx:=&*[coeffs[i]^sx[i]: i in [1..#s]];

    for i in D do
      res[i]+:=c * &*[L[k[z]]`coord[preims[i,z]]: z in [1..r]];
      resx[i]+:=cx * &*[L[kx[z]]`coord[preims[i,z]]: z in [1..r]]; // Here there was a typo, it should be cx
    end for;
  end for;


  y:=init_theta_point(resx);
  Q0:=init_theta_null_point(res);

  return y, Q0;

end function;