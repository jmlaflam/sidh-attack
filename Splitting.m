/* INPUTS: -- Z2: An associative array indexed by Z/2Z which contains
              the theta coordinates of level 2 of an elliptic curve E
  OUTPUTS: -- The elliptic curve E
           -- A list containing the roots of the polynomial f(x) where E: y^2=f(x)*/
function ThetaToEll(Z2)
    F<I>:= Parent(Z2[0]);
    Z2[1] := F ! Z2[1];
    
    e1:= (Z2[0]^2-Z2[1]^2)^2/(4*Z2[0]^2);
    e2:=F!0;
    e3:= -Z2[1]^2;
    
    R<x>:= PolynomialRing(F);
    
    f:=(x-e1)*(x-e2)*(x-e3);
    
    E:= EllipticCurve(f);
    
    return E, [e1,e2,e3];
end function;


 /* INPUTS: -- thP: the theta coordinates (in level 2) of a point P on an elliptic curve E 
               given by associative array indexed by Z/2Z
            -- thE: the theta coordinates (in level 2) of an elliptic curve E 
               given by associative array indexed by Z/2Z
           -- E: the elliptic curve E
           -- rac: the roots of the polynomial f(x) where E: y^2=f(x)
  OUTPUTS: -- The point P */
function ThetaToEllPt(thP,thE,E,rac)
    Ab:=AbelianGroup([2,2]);
    D:=Universe(thP`coord);

    thEan := AlgebraicToAnalyticThetaNullPoint(1,thE,Ab);
    thPan := AlgebraicToAnalyticThetaPoint(1,thE,thP,Ab);
    
    
    u,v2 := Level2ThetaPointToMumford(1,rac,thEan,thPan);
    if u eq 1 then
        P := E!0;
    else
        x:=Roots(u)[1][1];
        v:=Sqrt(v2);
        y:=Evaluate(v,x);
        P:= E![x,y];
    end if;
    return P;
end function;